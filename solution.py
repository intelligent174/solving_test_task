import psycopg2

from psycopg2 import Error

from config import HOST
from config import USER
from config import PASSWORD
from config import DB_NAME
from exceptions import PostgreSQLError

try:
    with psycopg2.connect(
            host=HOST,
            user=USER,
            password=PASSWORD,
            database=DB_NAME
    ) as connection:
        cursor = connection.cursor()

        cursor.execute("""
            SELECT id, title, stock, price 
              FROM Product 
          ORDER BY stock > 0 DESC, price;
        """)

        sorted_goods = cursor.fetchall()

        for product in sorted_goods:
            print(f'{product=}')
        print()

        cursor.execute(
            """
              SELECT Product.id, Product.title, stock, price 
                FROM Product 
                     INNER JOIN Product_property_value AS PPV 
                     ON Product.id = PPV.product_id 
                     INNER JOIN Property 
                     ON PPV.property_id = Property.id 
               WHERE (Property.code = 'width' AND PPV.value IN ('4', '5')) 
                      OR (Property.code = 'height' AND PPV.value='5') 
                      OR (PPV.value='Красный')  
            GROUP BY 1 
              HAVING Count(*)=3; 
            """
        )

        red_goods = cursor.fetchall()

        for red_product in red_goods:
            print(f'{red_product=}')


except Error as e:
    raise PostgreSQLError from e
